<?php
	include 'config/db_config.php';

	mysql_set_charset("utf8");

	$user_id = $request->user_id;

	$query = mysql_query("SELECT * FROM `orders` INNER JOIN `order_items` ON `orders`.`order_id` = `order_items`.`order_id` INNER JOIN `product` ON `order_items`.`product_id` = `product`.`product_id` WHERE `orders`.`customer_id` = '$user_id' ORDER BY  `orders`.`order_id` DESC");

	$response['OrderList'] = array();

	while($row = mysql_fetch_array($query)){
		$product['order_id'] = $row['orders_id'];
		$product['cart_id'] = $row['id'];
		$product['c_id'] = $row['product_id'];
		$product['c_name'] = $row['product_name'];
		$product['c_desc'] = $row['product_specification'];
		$product['c_prize'] = $row['total_final'];
		//$product['c_scale'] = $row['c_scale'];
		$product['c_image'] = 'http://macaronpatisserie.in/uploads/product/main/'.$row['product_image'];
		$product['count'] = $row['quantity'];
		$product['ordered_date'] = date("D M j G:i:s",$row['ordered_on']);
		$product['delivery_date'] = $row['delivery_date'];
		$product['delivery_time'] = $row['delivery_time'];
		//$product['contact_number'] = $row['contact_number'];
		//$product['deliver_address'] = $row['deliver_address'];
		$product['order_status'] = $row['order_status'];
		
		array_push($response['OrderList'], $product);
	}

	echo json_encode(array('OrderList'=>$response['OrderList']));
?>