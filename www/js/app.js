// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','starter.factories','starter.filters','ngStorage','ngMessages','ionic-datepicker','ionic-timepicker','angular-loading-bar','ngCordova'])

.run(function($ionicPlatform,$localStorage) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive

  // Each tab has its own nav history stack:

  .state('home', {
    url: '/home',
    cache : false,
        templateUrl: 'templates/home.html',
        controller: 'HomeCntrl'
  })
  .state('cakeList', {
    url: '/cakeList',
    cache : false,
        templateUrl: 'templates/cake_list.html',
        controller: 'cakeListCntrl'
  })
  .state('cakeDetails', {
    url: '/cakeDetails',
    cache : false,
        templateUrl: 'templates/cake_details.html',
        controller: 'cakeDetailsCntrl'
  })
  .state('login', {
    url: '/login',
    cache : false,
        templateUrl: 'templates/login.html',
        controller: 'loginCntrl'
  })
  .state('register', {
    url: '/register',
        templateUrl: 'templates/register.html',
        controller: 'registerCntrl'
  })
  .state('acntInfo', {
    url: '/acntInfo',
    cache:false,
        templateUrl: 'templates/account_info.html',
        controller: 'acntInfoCntrl'
  })
  .state('addAddrs', {
    url: '/addAddrs',
        templateUrl: 'templates/add_address.html',
        controller: 'addAddrsCntrl'
  })
  .state('orderDetails', {
    url: '/orderDetails',
    cache : false,
        templateUrl: 'templates/order-details.html',
        controller: 'orderDetailsCntrl',
        params: {myParam: null}
  })
  .state('orderSuccess', {
    url: '/orderSuccess',
        templateUrl: 'templates/order_success_page.html',
        controller: 'orderSuccessCntrl'
  })
  .state('userCart', {
    url: '/userCart',
    cache : false,
        templateUrl: 'templates/user_cart.html',
        controller: 'userCartCntrl'
  })
  .state('searchResult', {
    url: '/searchResult',
    cache : false,
        templateUrl: 'templates/search_result.html',
        controller: 'searchResultCntrl',
        params: {myParam: null}
  })
  .state('orderListDetails', {
    url: '/orderListDetails',
    cache : false,
        templateUrl: 'templates/user_order_listing.html',
        controller: 'orderListDetailsCntrl'
  })
  .state('changePassword', {
    url: '/changePassword',
    cache : false,
        templateUrl: 'templates/change_password.html',
        controller: 'changePasswordCntrl'
  })
  .state('usrAdrsList', {
    url: '/usrAdrsList',
    cache : false,
        templateUrl: 'templates/user_address_list.html',
        controller: 'usrAdrsListCntrl'
  })
  .state('addressSelection', {
    url: '/addressSelection',
    cache : false,
        templateUrl: 'templates/address_selection.html',
        controller: 'addressSelectionCntrl'
  })
  .state('editAddrs', {
    url: '/editAddrs',
    cache : false,
        templateUrl: 'templates/edit_address.html',
        controller: 'editAddrsCntrl'
  })
  .state('forgotPassword', {
    url: '/forgotPassword',
    cache : false,
        templateUrl: 'templates/forgot_password.html',
        controller: 'forgotPasswordCntrl'
  })
  .state('subCategories', {
    url: '/subCategories',
    cache : false,
        templateUrl: 'templates/sub_categories.html',
        controller: 'subCategoriesCntrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

   // $urlRouterProvider.otherwise(function($injector, $location){
   //   var state = $injector.get('$state');

   //      if(window.localStorage['loginStatus'] === "true") {
   //            state.go('home');
   //      }
   //          return $location.path();
   //        })

})

// Configure time picker
.config(function (ionicTimePickerProvider) {
  var timePickerObj = {
    inputTime: (((new Date()).getHours() * 60 * 60) + (0)),
    format: 12,
    step: 10,
    setLabel: 'Set',
    closeLabel: 'Close'
  };
  ionicTimePickerProvider.configTimePicker(timePickerObj);
  
  
});