var cakes = angular.module('starter.controllers', []);

//var ip = "localhost/Cake_Bakery_01/bin/php/";
//var ip = "192.168.2.11/Cake_Bakery/bin/php/";
//var ip = "www.macaron-patisserie.com//app/php/";
var ip = "macaronpatisserie.in/app/php/";

cakes.controller('HomeCntrl', function($scope,$cordovaToast,$timeout,$ionicPlatform,WebService,$state,$localStorage,$ionicModal,$rootScope) {

  $scope.networkError = false;
  $scope.user = {};
  $scope.loginStatus = false;
  $scope.internetError = false;

  $ionicModal.fromTemplateUrl('templates/user_search.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.reloadPage = function(){
    $state.reload();
  }

  //Checking login status and set cart count and profile icon
  if(window.localStorage['loginStatus']==='true'){
    console.log($localStorage.userData);
    $scope.loginStatus = true;
  }
  if(angular.isArray($localStorage.cartItems)){
      $rootScope.cartCount = $localStorage.cartItems.length;
    }
    else{
      $rootScope.cartCount = 0;
      $localStorage.cartItems = [];
    }

  //listing all cake types
  WebService.invokeService('login','POST','http://'+ip+'cake_type_list.php')
  .success(function(data){
    $scope.CakeTypes = data.CakeTypes;
    console.log($scope.CakeTypes);
    console.log(data.CakeTypes);

  })
  .error(function(data){
    console.log("Internet Error"); 
    $scope.internetError = true;
  })

  $scope.CakeList = function(id,name){
    console.log(id);
    $localStorage.cake_type_id = id;
    $localStorage.cake_type_name = name;
    $state.go('subCategories');
  }

  //direct to login page
  $scope.loginPage = function(){
      //Checking user is already logged in
    if(window.localStorage['loginStatus']==='true'){
          $state.go('acntInfo');

    }
    else{
          $state.go('login');
    }

  }

  //direct cart page
  $scope.goToCart = function(){
    //Checking user is already logged in

    if(window.localStorage['loginStatus']==='true'){
          $state.go('userCart');
    }
    else{
          $state.go('login');
    }
  }

  //open search model
  $scope.searchCakes = function(){
    $scope.modal.show()
    // call all tags in single list
      WebService.invokeService('login','POST','http://'+ip+'search-tags.php')
      .success(function(data){
        $scope.searchTags = data.searchTags;
        console.log($scope.searchTags);
      })
      .error(function(){
        console.log("Internet Failure");
      })
  }
  //direct to search result page
  $scope.userSearchCakes = function(form,key){
    if(form.$valid){
       $scope.modal.hide()
       //sending search key as params
      $state.go('searchResult', {myParam:key})
    }
  }

  //tags updated on change key word
  $scope.updateTags = function(key){

    var data = {
      key:key
    }
    var jsonStr = angular.toJson(data);
     WebService.invokeService('login','POST','http://'+ip+'search-tags.php',jsonStr)
      .success(function(data){
        $scope.searchTags = data.searchTags;
        console.log($scope.searchTags);
      })
      .error(function(){
        console.log("Internet Failure");
      })
  }
  // selection from tags and direct to search result page
  $scope.setSearchTag = function(value){
    $scope.searchString = value;
    $scope.modal.hide();
     //sending search key as params
    $state.go('searchResult', {myParam:value})
  }

  $scope.reloadPage = function(){
    $state.reload();
  }

  var exit = 0;
  $ionicPlatform.registerBackButtonAction(function () {
    if(exit == 0){
      $cordovaToast.showShortCenter('Press again to exit!');
      exit++;
       $timeout( function(){ exit = 0; }, 3000);
    }
    else{
      exit = 0;
      ionic.Platform.exitApp();
    }
  }, 100);

})

cakes.controller('subCategoriesCntrl',function($scope,$cordovaToast,$localStorage,WebService,$state,$stateParams, $ionicPlatform,$ionicHistory){
    
    $scope.sub_category_name = $localStorage.cake_type_name;

  var data = {
      category_id:$localStorage.cake_type_id
  }
  var jsonStr =  angular.toJson(data)
  WebService.invokeService('login','POST','http://'+ip+'cake_sub_categories.php',jsonStr)
  .success(function(data){
    $scope.SubCategories = data.SubCategories;
    console.log($scope.SubCategories);

  })
  .error(function(data){
    $scope.internetError = true;
    console.log("Internet Error");
  })

  $scope.CakeList = function(id,name){
    console.log(id);
    $localStorage.cake_sub_type_id = id;
    $localStorage.cake_sub_type_name = name;
    $state.go('cakeList');
  }
  $scope.goToCart = function(){
    //Checking user is already logged in

    if(window.localStorage['loginStatus']==='true'){
          $state.go('userCart');
    }
    else{
          $state.go('login');
    }
  }

  $scope.reloadPage = function(){
    $state.reload();
  }
    $scope.goBack = function(){
      $state.go('home')
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
})

cakes.controller('cakeListCntrl', function($scope,$cordovaToast,WebService, $ionicPlatform,$localStorage,$state,$ionicHistory ) {

    $scope.sub_category_name = $localStorage.cake_sub_type_name;
  //Listing th cake with the type id
  var data = {
      type_id:$localStorage.cake_sub_type_id
  }
  var jsonStr =  angular.toJson(data)
  WebService.invokeService('login','POST','http://'+ip+'cake-list.php',jsonStr)
  .success(function(data){
    $scope.CakeList = data.CakeList;
    console.log($scope.CakeList);
    console.log(data.CakeList);

  })
  .error(function(data){
    $scope.internetError = true;
    console.log("Internet Error");
  })

  $scope.CakeDetailsPage = function(cake){

    $localStorage.cake_Details = cake;
    $state.go('cakeDetails');

  }

  $scope.goToCart = function(){
    //Checking user is already logged in

    if(window.localStorage['loginStatus']==='true'){
          $state.go('userCart');
    }
    else{
          $state.go('login');
    }
  }
   $scope.reloadPage = function(){
    $state.reload();
  }

  $scope.goBack = function(){
    $state.go('subCategories');
  }

  $ionicPlatform.registerBackButtonAction(function () {
    $ionicHistory.goBack();
  }, 100);


})
cakes.controller('cakeDetailsCntrl',function($scope,ionicDatePicker,ionicTimePicker,dropdown,$cordovaToast,$ionicPopover, $ionicPlatform,WebService,$localStorage,$state,$rootScope,$ionicHistory,$filter){
    //Selected Cake Details
    $scope.cakeDetails = {};
    $scope.cakeDetails = $localStorage.cake_Details;
    $scope.cakeDetails.eggless = false;
    if(angular.isNumber($localStorage.cake_Details.product_size)){
      console.log("true")
      $scope.cakeDetails.weight = $localStorage.cake_Details.product_size;
    }
    else{
      console.log("false")
      $scope.cakeDetails.weight = 1000;
    }
    var cakePrice = $scope.cakeDetails.c_prize;
    $scope.cakeDetails.c_prize = cakePrice;
    console.log($scope.cakeDetails);
    console.log($scope.cakeDetails.c_scale)
    if($scope.cakeDetails.c_scale != null && $scope.cakeDetails.c_scale != undefined){
      $scope.weightData = $scope.cakeDetails.c_scale.split(',');
    }
    else{
      console.log($scope.cakeDetails.c_scale)
    }
    
    console.log($scope.weightData)
    var cakeDetails = $scope.cakeDetails;
    $scope.cakeCartStatus = function(){
      if($localStorage.cartItems){
        $scope.cakeInCartStatus = false;
        var cartItems = $localStorage.cartItems;
        //console.log($localStorage.cartItems)
        angular.forEach(cartItems,function(cartItems){
          if(cartItems.c_id == cakeDetails.c_id){
            $scope.cakeInCartStatus = true;
          }
        })
      }
      if($scope.cakeInCartStatus != true){
        return "Add To Cart";
      }
      else{
        return "Check Out";
      }
    }

     // $ionicPopover.fromTemplateUrl('templates/countPopover.html', {
     //    scope: $scope
     //  }).then(function(popover) {
     //    $scope.popover = popover;
     //  });

     // $scope.openPopover = function($event) {
     //    $scope.popover.show($event);
     //    $scope.orderItemCount = index;
     //    console.log(index);
     //  };

     //  $scope.setCount = function(value){
     //    var index = $scope.orderItemCount;
     //    console.log(value+','+index);
     //    $scope.cakeDetails.count = value;
     //    $scope.popover.hide();

     //  }

    //Assign Primary Count to the cake details
    $scope.cakeDetails.count = 1;

    //Define Array to store cake's details
    $localStorage.orderItems = [];
    console.log($localStorage.cartItems)


      //Set tommarow's date as first date for select date
  $scope.startDate = new Date();
  $scope.startDate.setDate($scope.startDate.getDate() + 1);

  //Object for datepicker
    var ipObj1 = {
      callback: function (val) {  //Mandatory
        $scope.deliveryDate = $filter('date')(new Date(val), 'dd/MM/yyyy');;
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
      },
      disabledDates: [],//optional
      from: $scope.startDate, //Optional
      //to: new Date(2017, 10, 30), //Optional
      inputDate: $scope.startDate,      //Optional
      mondayFirst: true,          //Optional
      disableWeekdays: [],       //Optional
      closeOnSelect: false,       //Optional
      templateType: 'popup'       //Optional
    };

    //object for time picker
    var ipObj = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          $scope.deliveryTime=dropdown.convertTo12(selectedTime.getUTCHours(),selectedTime.getUTCMinutes());
          //$scope.deliveryTime = val;
        }
      },
      inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
      format: 12,         //Optional
      step: 5,           //Optional
      setLabel: 'Set Time'    //Optional
    };

    //Open Date Picker
   $scope.openDatePicker = function(){
      ionicDatePicker.openDatePicker(ipObj1);
    };

    //Open Time Picker
    $scope.openTimePicker = function(){
      ionicTimePicker.openTimePicker(ipObj);
    };

    //Add cake to user cart
    $scope.addToCart = function(){
      //Check the login status
      if(window.localStorage['loginStatus'] ==='true'){

        if($scope.deliveryTime == undefined || $scope.deliveryDate == undefined){
             $cordovaToast.showLongBottom('please selecte your delivery time and date').then(function(success) {
                // success
              }, function (error) {
                // error
              });
          }
          else{
            if($scope.checkCartItems($scope.cakeDetails)){
              if($localStorage.cartId){
                  var data = {
                    cart_id : $localStorage.cartId,
                    cake_id : $scope.cakeDetails.c_id,
                    count :$scope.cakeDetails.count,
                    prize : $scope.cakeDetails.c_prize,
                    deliveryDate : $scope.deliveryDate,
                    deliveryTime : $scope.deliveryTime,
                    cake_weight : $scope.cakeDetails.weight
                  }
                  var jsonStr = angular.toJson(data);
                  WebService.invokeService('login','POST','http://'+ip+'UpdateToCart.php',jsonStr)
                  .success(function(data){
                    $localStorage.cartItems.push($scope.cakeDetails);
                    $rootScope.cartCount = $localStorage.cartItems.length;
                    $cordovaToast.showShortCenter($scope.cakeDetails.c_name+' is added to cart');
                   // $state.go('userCart'); 
                  })
                  .error(function(){
                    console.log("Internet Failure");
                  })
              }
              else{
                  console.log($scope.cakeDetails.eggless);
                  var data = {
                    cake_id : $scope.cakeDetails.c_id,
                    count :$scope.cakeDetails.count,
                    prize : $scope.cakeDetails.c_prize,
                    deliveryDate : $scope.deliveryDate,
                    deliveryTime : $scope.deliveryTime,
                    cake_weight : $scope.cakeDetails.weight
                  }
                  var jsonStr = angular.toJson(data);
                  WebService.invokeService('login','POST','http://'+ip+'addToCart.php',jsonStr)
                  .success(function(data){
                    $localStorage.cartId = data.cart_id;
                    $localStorage.cartItems.push($scope.cakeDetails);
                    $rootScope.cartCount = $localStorage.cartItems.length;
                    $cordovaToast.showShortCenter($scope.cakeDetails.c_name+' is added to cart');
                    //$state.go('userCart');
                  })
                  .error(function(){
                    console.log("Internet Failure");
                  })
              }
          }
          else{
             $state.go('userCart'); 
             console.log('Allready in cart');
          }
        }  
    }
    else{
        //user is not logged in
        $state.go('login');
      }

  }

   // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('templates/weightPopover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openWeigthPopover = function($event) {
    $scope.popover.show($event);
  };

  $scope.setItemScale = function(value){
    console.log(cakePrice);
    console.log($scope.cakeDetails)
    $scope.cakeDetails.c_prize = (parseFloat(value)*parseFloat($scope.cakeDetails.c_prize))/parseFloat($scope.cakeDetails.weight);
    $scope.cakeDetails.weight = value;
    $scope.popover.hide();
  }

    $scope.checkCartItems = function(cakeDetails){
      var flag = 0;
      console.log($localStorage.cartItems);
      if($localStorage.cartItems){
        for (var i = 0; i < $localStorage.cartItems.length; i++) {
          console.log($localStorage.cartItems);
          if($localStorage.cartItems[i].c_id == cakeDetails.c_id){
            flag++
          }
        }
        console.log(flag)
      }
      if(flag == 0){
        return true;
      }
      else{
        return false;
      }
    }


    $scope.goToCart = function(){
    //Checking user is already logged in

    if(window.localStorage['loginStatus']==='true'){
          $state.go('userCart');
    }
    else{
          $state.go('login');
    }
  }


    $scope.goBack = function(){
      $ionicHistory.goBack();
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack();
      }, 100);
})
cakes.controller('loginCntrl',function($scope,$cordovaToast,WebService, $ionicPlatform,$localStorage,$rootScope,$state,$ionicHistory ){
  

  $localStorage.loginLastView = $ionicHistory.backView();

  $scope.loginData = {};
  $scope.loginError = false;
  $scope.LoginFormShow = true;
  $scope.OTPFormShow = false;
  $scope.forgotMsg = '';

  $scope.login = function(form){

    //Check form is valid of not
    if(form.$valid){
      $scope.loginError = false;
      //login api is calling
      var jsonStr = angular.toJson($scope.loginData);

      WebService.invokeService('login','POST','http://'+ip+'login.php',jsonStr)
      .success(function(data){

        if(data.result == 1){
          console.log(data)
          window.localStorage['loginStatus'] = true;
          $localStorage.userData = data;
          $ionicHistory.goBack();
        }
        else{
          $scope.loginError = true;
          console.log('Incorrect username or password');
        }
      })
      .error(function(){
        console.log("Internet Failure");
      })
    }
  }

  $scope.resendOtp = function(){
    if($scope.loginData.phone){
      var data = {
        number : $scope.loginData.phone
      }
      var jsonStr = angular.toJson(data);
      WebService.invokeService('login','POST','http://'+ip+'otp_login.php',jsonStr)
      .success(function(data){
        if(data.result == 1){
          $scope.OTPFormShow = true;
          $scope.LoginFormShow = false;
          console.log("OTP Request send");
          $cordovaToast.showLongBottom('OTP Request send, Please check your inbox');
        }
        else{
          $scope.forgotMsg = 'we can not find this number';
          console.log("not a registered user");
          $cordovaToast.showLongBottom('we can not find this number');
        }
      })
      .error(function(){
        console.log("Internet Failure");
      })

    }
    else{
      $scope.forgotMsg = 'Please enter a valid number';
      console.log("undefined :"+$scope.loginData.phone)
      $cordovaToast.showLongBottom("Please enter a valid number");
    }
  }

      $scope.OtpVerification = function(form){
        console.log("here")
      if(form.$valid){

        var jsonStr = angular.toJson($scope.loginData)

        WebService.invokeService('login','POST','http://'+ip+'login_otp_verification.php',jsonStr)
        .success(function(data){
          if(data.result == 0){
            //$state.go('home');
            window.localStorage['loginStatus'] = true;
            $localStorage.userData = {};
            $localStorage.userData = data;
            $scope.otpError = false;
            $scope.LoginFormShow = true;
            $scope.OTPFormShow = false;
            console.log($localStorage.loginLastView);
            $ionicHistory.goBack();
          }
          if(data.result == 1){
            $scope.otpError = true;
          }
        })
        .error(function(){
          console.log("Internet Failure");
        })
      }
    }

  $scope.editNumber = function() {
      $scope.OTPFormShow = false;
      $scope.LoginFormShow = true;
  }

  $scope.goBack = function(){
    $ionicHistory.goBack();
  }

  $ionicPlatform.registerBackButtonAction(function () {
    $ionicHistory.goBack();
  }, 100);

})
cakes.controller('changePasswordCntrl',function($scope,$cordovaToast, $ionicPlatform,$localStorage,$state,WebService,$ionicHistory){

    $scope.password = {};
    $scope.password.user_id = $localStorage.userData.user_id;
    $scope.passMismatch = false;

    $scope.save = function(form){
      if(form.$valid){
        if($scope.password.confirm_new_password == $scope.password.new_password){

            var jsonStr = angular.toJson($scope.password);
            WebService.invokeService('login','POST','http://'+ip+'updatePassword.php',jsonStr)
            .success(function(data){
              if(data.result == 1){
                //console.log("password change successfully");
                $cordovaToast.showLongBottom('password change successfully');
                $state.go('acntInfo');
              }
              else{
                console.log("ENtered Incorrect password")
                $cordovaToast.showLongBottom('ENtered Incorrect password').then(function(success) {
                   // success
                }, function (error) {
                  // error
                });
              }
            })
            .error(function(){
              console.log("Internet Failure");
            })
        }
        else{
              $scope.passMismatch = true;
        }
      }
    }

    $scope.goBack = function(){
      $state.go('acntInfo');
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $state.go('acntInfo')
      }, 100);

})
cakes.controller('registerCntrl',function($scope,$cordovaToast,WebService, $ionicPlatform,$localStorage,$state,$ionicHistory){

    $scope.registerData = {};
    $scope.RegFormShow = true;
    $scope.OTPFormShow = false;
    $scope.numberExist = false;
    $scope.blockNumError = false;
    $scope.otpError = false;
    $scope.otpHeadMessage = "Enter the OTP that you have recieved in the registered number to activate your account";

    //Register user account using phone number & Password
    $scope.registerAccount = function(form){

      if(form.$valid){
        var jsonStr = angular.toJson($scope.registerData)

        WebService.invokeService('login','POST','http://'+ip+'registration.php',jsonStr)
        .success(function(data){
          console.log(data);
          //Success fully registered
          if(data.result == 0){
            $scope.RegFormShow = false;
            $scope.otpcode = data.otp;
            $scope.OTPFormShow = true;
          }
          if(data.result == 2){
            $scope.otpHeadMessage = "The number is already registered, But not verified. Please enter the OTP to complete registration";
            $scope.RegFormShow = false;
            $scope.otpcode = data.otp;
            $scope.OTPFormShow = true;
          }
          //Active user
          if(data.result == 1){
              $scope.numberExist = true;
          }
        })
        .error(function(){
          console.log("Internet Failure");
        })
      }
    }

      $scope.resendOtp = function(){

        if($scope.registerData.phone){
          var data = {
            number : $scope.registerData.phone
          }
          var jsonStr = angular.toJson(data);
          WebService.invokeService('login','POST','http://'+ip+'resend_otp.php',jsonStr)
          .success(function(data){
            if(data.result == 1){
              $scope.OTPFormShow = true;
              $scope.LoginFormShow = false;
              console.log("OTP Request send");
            }
            else{
              $scope.forgotMsg = 'we can not find this number';
              console.log("not a registered user");
              $cordovaToast.showLongBottom('we can not find this number');
            }
          })
          .error(function(){
            console.log("Internet Failure");
          })

        }
        else{
          console.log("undefined :"+$scope.loginData.phone)
        }
  }


    // otp Verification function
    $scope.OtpVerification = function(form){
      if(form.$valid){

        var jsonStr = angular.toJson($scope.registerData)

        WebService.invokeService('login','POST','http://'+ip+'verifyOtp.php',jsonStr)
        .success(function(data){
          if(data.result == 0){
            //$state.go('home');
            window.localStorage['loginStatus'] = true;
            $localStorage.userData = {};
            $localStorage.userData = data;
            $scope.otpError = false;
            $scope.RegFormShow = true;
            $scope.OTPFormShow = false;
              switch($localStorage.loginLastView.stateName){
                case 'home'         : $state.go('home');
                                      break;
                case 'cakeDetails'  : $state.go('cakeDetails');
                                      break;      
                default             :    $state.go('home');                   
              }
          }
          if(data.result == 1){
            $scope.otpError = true;
          }
        })
        .error(function(){
          console.log("Internet Failure");
        })
      }
    }

    $scope.EditNumber = function(){
      $scope.otpError = false;
      $scope.RegFormShow = true;
      $scope.OTPFormShow = false;
    }

    $scope.goBack = function(){
      $ionicHistory.goBack();
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack();
      }, 100);

})
cakes.controller('acntInfoCntrl',function($scope,$cordovaToast, $ionicPlatform,$localStorage,WebService,$timeout,$state,$ionicHistory,$ionicModal){


    $scope.userData = $localStorage.userData;
    console.log($scope.userData)

    $ionicModal.fromTemplateUrl('templates/personalDetails.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
     
    };

    if($scope.userData.name == null ||$scope.userData.name == ''||$scope.userData.name == undefined){
      console.log($scope.modal)
      $timeout(function() {
        if($scope.modal){
            console.log($scope.modal)
          $scope.modal.show();
        }
      }, 100);
      
    }
    if($scope.userData.email == null ||$scope.userData.email == ''||$scope.userData.email == undefined){
      console.log('No email')
    }
    if($scope.userData.pincode == null ||$scope.userData.pincode == ''||$scope.userData.pincode == undefined){
      console.log('No pincode')
    }

    $scope.updateProfile = function(form){
      if(form.$valid){
        var jsonStr = angular.toJson($scope.userData);
        WebService.invokeService('login','POST','http://'+ip+'updateProfileInfo.php',jsonStr)
          .success(function(data){
            $scope.modal.hide();
          })
          .error(function(){
            console.log("Internet Failure");
          })
      }
    }

    

    $scope.UserOrders = function(){
      $state.go('orderListDetails');
    }

    $scope.UserAddress = function(){
      $state.go('usrAdrsList');
    }

    $scope.UserChangePassword = function(){
      $state.go('changePassword');
    }

    $scope.logout = function(){
        $state.go('home');
        $localStorage.cartItems = [];
        $localStorage.cartId = '';
        $ionicHistory.clearCache();
        $localStorage.$reset();
        window.localStorage['loginStatus'] = false;
    }

    $scope.checkIonHistory = function(){
      console.log($ionicHistory);
      console.log($ionicHistory.backTitle ());
    }

    $scope.goBack = function(){
      $state.go('home');
    }

    $ionicPlatform.registerBackButtonAction(function () {
             $state.go('home');
      }, 100);

})
cakes.controller('usrAdrsListCntrl',function($scope,$cordovaToast,$ionicPopup, $ionicPlatform,$localStorage,$state,WebService,$ionicHistory){

    var data = {
      user_id : $localStorage.userData.user_id
    }
    var jsonStr = angular.toJson(data);
    WebService.invokeService('login','POST','http://'+ip+'user_address_list.php',jsonStr)
    .success(function(data){
      $scope.addressList = data.addressList;
    })
    .error(function(){
      console.log("Internet Failure");
    })

    $scope.addNewAddress = function(){
      $state.go('addAddrs');
    }

    $scope.editAddress = function(index){
      console.log(index);
      $localStorage.passEditAddress = $scope.addressList[index]
      console.log($localStorage.passEditAddress)
     $state.go('editAddrs');
    }

     $scope.showalertPopup = function(id) {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Delete Address',
         template: 'Are you sure you want to delete this address?'
       });

       confirmPopup.then(function(res) {
         if(res) {
           console.log('You are sure');
           $scope.deleteAddress(id)
         } else {
           console.log('You are not sure');
         }
       });
     };

    $scope.deleteAddress = function(id){
        console.log(id);
        var data = {
          add_id : id,
          user_id : $localStorage.userData.user_id
        }
        var jsonStr = angular.toJson(data);
        WebService.invokeService('login','POST','http://'+ip+'delete_address.php',jsonStr)
          .success(function(data){
            $scope.addressList = data.addressList;
          })
          .error(function(){
            console.log("Internet Failure");
          })
    }

    $scope.goBack = function(){
        $state.go('acntInfo');
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $state.go('acntInfo')
      }, 100);
})
cakes.controller('orderSuccessCntrl',function($scope,$cordovaToast,$localStorage,$state ,$ionicPlatform,$ionicHistory){
  $scope.order_id = $localStorage.order_id;
  $scope.goHome = function(){
    $state.go('home');
  }

  $scope.order_id = $localStorage.order_id;

  $scope.goOrderList = function(){
      $state.go('orderListDetails');
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $state.go('home');
      }, 100);
})


cakes.controller('orderListDetailsCntrl',function($scope,$ionicPopup,$cordovaToast,$localStorage,$state,WebService, $ionicPlatform,$ionicHistory){
    var data = {
      user_id : $localStorage.userData.user_id
    }
    var jsonStr = angular.toJson(data);
    WebService.invokeService('login','POST','http://'+ip+'order_Details.php',jsonStr)
          .success(function(data){
            $scope.OrderList = data.OrderList;
          })
          .error(function(){
            console.log("Internet Failure");
          })

      $scope.shopNow = function(){
        $state.go('home');
      }

      $scope.showalertPopup = function(item) {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Cancel Order',
         template: 'Are you sure you want to cancel this order?'
       });

       confirmPopup.then(function(res) {
         if(res) {
           console.log('You are sure');
           $scope.cancelOrder(item)
         } else {
           console.log('You are not sure');
         }
       });
     };

      $scope.cancelOrder = function(item){
        console.log(item)
        var data = {
          cart_id : item.cart_id
        }
        var jsonStr = angular.toJson(data);
        WebService.invokeService('login','POST','http://'+ip+'delete_cart_product.php',jsonStr)
          .success(function(data){
            $state.reload();
          })
          .error(function(){
            console.log("Internet Failure");
          })
      }

      $scope.convertToAngularTime = function(value){
         return value * 1000
      }

     $scope.goBack = function(){
      $state.go('acntInfo');
     }     

    $ionicPlatform.registerBackButtonAction(function () {
        $state.go('acntInfo');
      }, 100);
})
cakes.controller('orderDetailsCntrl',function($scope,$cordovaToast,$ionicScrollDelegate,$localStorage,$ionicPopover,$stateParams,$state,WebService,$rootScope,dropdown,ionicTimePicker,ionicDatePicker,$ionicHistory, $ionicPlatform){
  //User Primary Address
  
  $scope.selectedAddressId = $stateParams.myParam;
  if($scope.selectedAddressId != 0 ){
    for (var i = 0; i < $localStorage.addressList.length; i++) {
        if($localStorage.addressList[i].address_id == $scope.selectedAddressId){
          $scope.addressList = $localStorage.addressList[i];
        }
        else{
          $scope.addressList = $localStorage.addressList[0];
        }
    }
  }
  else{
    $scope.addressList = $localStorage.addressList[0];
  }
  //User Ordered Or Carted Items
  $scope.orderItems = $localStorage.orderItems;
  $scope.deliveryCost = 100;
  console.log($scope.addressList);
  console.log($scope.orderItems);
//find total cost
  $scope.totalCost = function(){
    var cost = 0;
    for (var i = 0; i < $scope.orderItems.length; i++) {
      cost = cost + parseInt($scope.orderItems[i].c_prize) * parseInt($scope.orderItems[i].count);
    }
    return cost;
  }

  
  var total_submit_cost = $scope.totalCost()+$scope.deliveryCost;
  console.log(total_submit_cost);


  //confirm ordering and update th order
  $scope.confirmOrder = function(){
      //Update Order Details
      var data = {
        user_id:$localStorage.userData.user_id,
        address : $scope.addressList,
        deliveryDate : $scope.deliveryDate,
        deliveryTime : $scope.deliveryTime,
        cart_id :$localStorage.cartId,
        total_cost : total_submit_cost
      };

      var jsonStr = angular.toJson(data);
      WebService.invokeService('login','POST','http://'+ip+'orderItem.php',jsonStr)
          .success(function(data){
            $localStorage.order_id = data.order_id;
            $localStorage.cartId = '';
            $localStorage.cartItems = [];
            $localStorage.orderItems = [];
            console.log('000'+$localStorage.order_id);
            // if($ionicHistory.backTitle () == 'Cart Items'){
            //   console.log($ionicHistory.backTitle ());
            //   $localStorage.cartItems = [];
            //   $localStorage.userData.cart_count = $localStorage.cartItems.length;
            // }
            $state.go('orderSuccess');
          })
          .error(function(){
            console.log("Internet Failure");
          })

  }

    $scope.check = function(data){
      console.log(data);
    }

      $scope.viewPriceDetails = function(){
        $ionicScrollDelegate.scrollBottom();
      }

      $scope.ChangeAddress = function(){
        $state.go('addressSelection');
      }


      $scope.goBack = function(){
        $ionicHistory.goBack()
      }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
})
cakes.controller('addAddrsCntrl',function($scope,$cordovaToast,WebService ,$localStorage,$state,$ionicHistory, $ionicPlatform){

    $scope.Newaddress = {};
    $scope.Newaddress.user_id = $localStorage.userData.user_id;

  $scope.AddNewAddress = function(form){
    console.log("here")
      if(form.$valid){
          var jsonStr = angular.toJson($scope.Newaddress);
          WebService.invokeService('login','POST','http://'+ip+'add_user_address.php',jsonStr)
        .success(function(data){
          $localStorage.addressList = data.addressList;
         // $state.go('orderDetails');
          $ionicHistory.goBack();
        })
        .error(function(){
          console.log("Internet Failure");
        })
      }
  }

  $scope.pincheck = function(value){
      if(value === undefined){
         console.log(value);
      }
      else{
         //console.log(value.toString().length);
         var data = {
              pin : value
            }
            var jsonStr = angular.toJson(data);

           WebService.invokeService('login','POST','http://'+ip+'pincheck.php',jsonStr)
           .success(function(data){
            console.log(data);
            if(data.result == 1){
              $scope.pincodeErrorMessage = false;
              $scope.pincodeErrorMessage2 = true;
            }
            else{
              $scope.pincodeErrorMessage = true;
              $scope.pincodeErrorMessage2 = false;
            }
            

           })
           .error(function(){
            $cordovaToast.showShortCenter('Network Error');
           })
      }
     
    }

  $scope.goBack = function(){
    $ionicHistory.goBack();
  }
    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
})

cakes.controller('addressSelectionCntrl',function($scope,$cordovaToast,$localStorage,WebService,$state, $ionicPlatform,$ionicHistory){
  $scope.addressList = $localStorage.addressList;
  $scope.user = {}
  $scope.user.selectedAddressId = $scope.addressList[0].address_id;

  $scope.addNewAddress = function(){
    $state.go('addAddrs');
  }

  $scope.editAddress = function(index){
      console.log(index); 
      $localStorage.passEditAddress = $scope.addressList[index]
      console.log($localStorage.passEditAddress)
     $state.go('editAddrs');
  }

  $scope.doneAddressSelection = function(value){
    console.log(value);
    if($scope.user.selectedAddressId != 0){
       $state.go('orderDetails', {myParam:$scope.user.selectedAddressId})
    }
    else{
      console.log("Please Select a address")
    }
  }

  $scope.goBack = function(){
    if($scope.user.selectedAddressId != 0){
       $state.go('orderDetails', {myParam:0})
    }
    else{
      console.log("Please Select a address")
    }
  }

    $ionicPlatform.registerBackButtonAction(function () {
        if($scope.user.selectedAddressId != 0){
           $state.go('orderDetails', {myParam:0})
        }
        else{
          console.log("Please Select a address")
        }
      }, 100);
})

cakes.controller('editAddrsCntrl',function($scope,$cordovaToast,$localStorage,WebService,$state,$ionicPlatform,$ionicHistory){
  $scope.editAddrs = $localStorage.passEditAddress;
  $scope.editAddrs.pincode = parseInt($scope.editAddrs.pincode);
  $scope.editAddrs.p_number = parseInt($scope.editAddrs.p_number);

  console.log($scope.editAddrs)

  $scope.editAddrs.user_id = $localStorage.userData.user_id;

  $scope.AddEditAddrs = function(form){
    if(form.$valid){
          var jsonStr = angular.toJson($scope.editAddrs);
          WebService.invokeService('login','POST','http://'+ip+'update_address.php',jsonStr)
          .success(function(data){
            $ionicHistory.goBack();
          })
          .error(function(){
            console.log("Internet Failure");
          })
      }
  }

  $scope.goBack = function(){
    $ionicHistory.goBack();
  }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
})
cakes.controller('userCartCntrl',function($scope,$cordovaToast,$ionicScrollDelegate,WebService,$rootScope,$ionicPopover ,$localStorage,$state, $ionicPlatform,$ionicHistory){

  $scope.cartItems = [];
  $scope.deliveryCost = 100;
  $scope.emptyCart = false;

    var data = {
      user_id :$localStorage.userData.user_id
    }

    if ($localStorage.cartId!='' && $localStorage.cartId != undefined) {
      $scope.cartItems = $localStorage.cartItems;
      if($localStorage.cartItems.length == 0){
          console.log("cart is empty")
          $scope.emptyCart = true;
        }
        else{
          console.log($localStorage.cartItems)
          console.log("here")
        }
    }
    else{
          console.log("cart is empty")
          $scope.emptyCart = true;
      
    }

  $scope.shopNow = function(){
    $state.go('home');
  }

  $scope.goHome = function(){
    $state.go('home');    
  }

 

  $ionicPopover.fromTemplateUrl('templates/countPopover.html', {
        scope: $scope
      }).then(function(popover) {
        $scope.popover = popover;
      });

  $scope.openPopover = function($event,index) {
        $scope.popover.show($event);
        $scope.orderItemCount = index;
        console.log(index);
      };

      $scope.setCount = function(value){
        var index = $scope.orderItemCount;
        console.log(value+','+index);
        console.log($scope.cartItems);
        if($scope.cartItems[index].count == value){
          console.log("No Change");
        }
        else{
          $scope.cartItems[index].count = value;
          console.log($scope.cartItems[index].count)
          console.log(value)
          $scope.changeItemCount(value);
        }
        $scope.changeItemCount(value);
        $scope.popover.hide();

      }

      //find total cost
      $scope.totalCost = function(){
        var cost = 0;
        for (var i = 0; i < $scope.cartItems.length; i++) {
          cost = cost + parseInt($scope.cartItems[i].c_prize) * parseInt($scope.cartItems[i].count);
        }
        return cost;
      }

      $scope.placeOrder = function(){
        $localStorage.orderItems = $scope.cartItems;
        //call address list of user
          var data = {
                user_id : $localStorage.userData.user_id
              }
              var jsonStr = angular.toJson(data);
              WebService.invokeService('login','POST','http://'+ip+'user_address_list.php',jsonStr)
              .success(function(data){
                console.log(data);
                if(data.addressList.length == 0){
                  //user not update address go to add address page
                  $state.go('addAddrs');
                }
                else{
                  //Already updated go to summary page
                   $localStorage.addressList = data.addressList;
                   console.log($localStorage.addressList);
                    $state.go('orderDetails',{myParam: 0});
                  }
              })
              .error(function(){
                console.log("Internet Failure");
              })
      }

      $scope.removeCartItems = function(item){
        console.log(item);
        var index = $scope.cartItems.indexOf(item);
        console.log(index);
        var data = {
          user_id : $localStorage.userData.user_id,
          c_id : item.c_id
        }
        var jsonStr = angular.toJson(data);
        WebService.invokeService('login','POST','http://'+ip+'remove_cart_item.php',jsonStr)
              .success(function(data){
                $scope.cartItems.splice(index, 1);
                $localStorage.cartItems = $scope.cartItems;
                $rootScope.cartCount = $scope.cartItems.length;
                console.log($rootScope.cartCount)
                if($rootScope.cartCount == 0){
                  console.log("cart is empty")
                    $scope.emptyCart = true;
                    $localStorage.cartId = null;
                }
                 $cordovaToast.showLongBottom('Item removed from cart').then(function(success) {
                   // success
                  }, function (error) {
                    // error
                  });
              })
              .error(function(){
                console.log("Internet Failure");
              })

      }

      $scope.changeItemCount = function(value){
        var index = $scope.orderItemCount;
        var c_id = $scope.cartItems[index].c_id;
        var total_cost = $scope.cartItems[index].c_prize * value;
        var data = {
          cart_id : $localStorage.cartId,
          c_id : c_id,
          count : value,
          prize : total_cost
        }
        var jsonStr = angular.toJson(data);
        WebService.invokeService('login','POST','http://'+ip+'update_item_count.php',jsonStr)
              .success(function(data){
              })
              .error(function(){
                console.log("Internet Failure");
              })
      }

      $scope.goBack = function(){
        $ionicHistory.goBack();
      }

       $scope.viewPriceDetails = function(){
        $ionicScrollDelegate.scrollBottom([true]);
      }

    $ionicPlatform.registerBackButtonAction(function () {
       $ionicHistory.goBack()
      }, 100);
})
cakes.controller('searchResultCntrl',function($scope,$cordovaToast,$localStorage,WebService,$state,$stateParams, $ionicPlatform,$ionicHistory){
  $scope.key = $stateParams.myParam;
  console.log($scope.key);

     console.log($scope.key);
      var data = {
        key : $scope.key
      };
      var jsonStr = angular.toJson(data);

      WebService.invokeService('login','POST','http://'+ip+'search.php',jsonStr)
      .success(function(data){
        $scope.CakeList = data.CakeList;
        console.log(data);
      })
      .error(function(){
        console.log("Internet Error");
      })


       $scope.CakeDetailsPage = function(cake){

          $localStorage.cake_Details = cake;
          $state.go('cakeDetails');

        }

        $scope.goHome = function(){
          $state.go('home');
        }

    $scope.goBack = function(){
      $state.go('home');
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
})
cakes.controller('forgotPasswordCntrl',function($scope,$cordovaToast,$localStorage,WebService,$state,$stateParams, $ionicPlatform,$ionicHistory){

    $scope.goBack = function(){
      $state.go('home');
    }

    $ionicPlatform.registerBackButtonAction(function () {
        $ionicHistory.goBack()
      }, 100);
});
