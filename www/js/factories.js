angular.module('starter.factories',[])
.factory('dropdown', function(WebService) {
    var factory = {}; 
    //Function to convert 24 to 12 hour format
    factory.convertTo12=function(hour,minute){
        var hour1=parseInt(hour);
        var minute1=parseInt(minute);
        var format="AM";
        if(hour1>12){
            format="PM";
            hour1=hour1-12;
        }
        if(hour1==12){
            format="PM"
        }
        if(hour1==0)
            hour1=12;
        var hour_s=""+hour1;
        var minute_s=""+minute1;
        if(hour1<10)
           hour_s="0"+hour_s;
        if(minute1<10)
            minute_s="0"+minute_s;
        return hour_s+" : "+minute_s+" "+format;
    }

    factory.rate_app=function(){
        if($localStorage.count==null){
            var count=0;
            $localStorage.count=count;
            console.log("here");
        }
        else{
            count=$localStorage.count;
            console.log("here1");
        }
        if($localStorage.count<=5){
        setTimeout(function(){

            var myPopup = $ionicPopup.show({
        title: 'Rate Maids On Time',
        subTitle: 'If you enjoy using  Maids On Time App would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!',
        buttons: [
          {  type: 'button-positive',
            text: 'No, Thanks' },
          {
        text: '<b>Rate Now</b>',
        type: 'button-positive',
        onTap: function(e) {
          $cordovaInAppBrowser.open('market://details?id=com.webshore.arivonline', '_system') ;
             }
            },
            {
             type: 'button-positive',
             text: 'Later'
              }]
             });
            count++;
            $localStorage.count=count;
            console.log($localStorage.count);
            },3000)
        }
    }
    return factory;
});