var filters = angular.module('starter.filters',[]);

filters.filter('searchFor', function(){
  return function(arr, searchString){
    if(!searchString){
      return arr;
    }
    var result = [];
    searchString = searchString.toLowerCase();
    angular.forEach(arr, function(item){
    	//console.log(searchString);
       console.log(item.tag)
       if(item.tag!=null &item.tag!=undefined){
          if(item.tag.toLowerCase().indexOf(searchString) !== -1){
            //console.log(item)
            result.push(item);
        }
       }
    });
    return result;
  };
})

filters.filter('htmlToPlaintext', function()
{
    return function(text)
    {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});