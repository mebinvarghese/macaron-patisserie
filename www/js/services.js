var srv = angular.module('starter.services', []);

srv.service('WebService', function($q,$http,$localStorage) {
    return {
        invokeService: function(from,method,url,data) {   
            var deferred = $q.defer();
            var promise = deferred.promise; 
            var myheaders = [];
            var obj = {'Content-Type': 'application/x-www-form-urlencoded'};
    
            if(from!='login'&&from!='register'){
                if($localStorage.token != null || $localStorage.token  !='') {
                    //alert($localStorage.token);
                    var obj = {'Content-Type': 'application/x-www-form-urlencoded',
            'login_token':  $localStorage.token};
                }
                else{
                    alert('Please login to continue');
                    $location.url('/login');
                }
            }        
            $http({
              method  : method,
              url     : url,
              data    : data, //forms user object   
              headers : obj
            })
            .success(function(data) {       
                deferred.resolve(data);
            })
            .error(function() { 
                deferred.reject('Something Went Wrong');
            });

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }           
            return promise;
        }
    }
});